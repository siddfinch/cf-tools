# cf-tools
Configuration Tools

Cf is a system configuration tool. Inspired by cfengine it provides convergent
manipulations of files, but adds a modular mechanism for building higher level
configuration operations, for example manipulating user accounts or cron
entries.

* Mat Kovach <mkovach@alal.com>

Well, somebody had to take over ... :) 

* First Plans
  * I need to document this a bit better. Once you get the hang of it it starts
to make sense. But the learning curve can be high.
  * I want to update the package stuff (apt, yum, zypper) to include a few 
more things (using yum security to apply just security related stuff, etc.)
  * Add OpenBSD related stuff. I will look at FreeBSD and NetBSD also, but 
I use OpenBSD. 
  * Clean up the account management stuff.
  *  Add a code release service.


